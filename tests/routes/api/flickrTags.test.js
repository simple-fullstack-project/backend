const chai = require('chai')
const chaiHttp = require('chai-http')
const { name, internet, lorem } = require('faker')
const ServerHelper = require('../../helpers/serverHelper')
const server = new ServerHelper()

chai.use(chaiHttp)
const { expect, request } = chai

before(async function () {
    await server.start()
    this.db = server.db
    this.app = server.app
    this.port = server.port
})
after(async function () {
    await server.drop()
})

let token;
describe('Flickr Route: Search tags', () => {
    it("should can get user token", async function () {
        const userBody = {
            fullName: name.findName(),
            email: internet.email(),
            password: internet.password(12),
            photoUrl: internet.avatar()
        }

        const register = await request(this.app)
            .post('/register')
            .send(userBody)
        expect(register.status).is.equal(200)
        token = register.body.token
    })

    it("should can get flickr feed with authorization", async function () {
        const tags = lorem.word()
        const response = await request(this.app)
            .get(`/flickr-search/${tags}`)
            .set('Authorization', 'Bearer ' + token)
        expect(response.status).is.equal(200)
    })

    it("should can't get flickr feed without tags", async function () {
        const tags = ''
        const response = await request(this.app)
            .get(`/flickr-search/${tags}`)
            .set('Authorization', 'Bearer ' + token)
        expect(response.status).is.equal(404)
    })

    it("should can't search flickr by tags without authorization", async function () {
        const tags = lorem.word()
        const response = await request(this.app)
            .get(`/flickr-search/${tags}`)
        expect(response.status).is.equal(401)
    })
})
