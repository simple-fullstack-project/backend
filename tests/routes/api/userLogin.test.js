const chai = require('chai')
const chaiHttp = require('chai-http')
const { name, internet } = require('faker')
const ServerHelper = require('../../helpers/serverHelper')
const server = new ServerHelper()

chai.use(chaiHttp)
const { expect, request } = chai

before(async function () {
    await server.start()
    this.db = server.db
    this.app = server.app
    this.port = server.port
})
after(async function () {
    await server.drop()
})

describe('User Route: Login', () => {

    it("should can login", async function () {
        const userBody = {
            fullName: name.findName(),
            email: internet.email(),
            password: internet.password(12)
        }

        const register = await request(this.app)
            .post('/register')
            .send(userBody)

        const response = await request(this.app)
            .post('/login')
            .send(userBody)
        expect(response.status).is.equal(200)
    })

    it(`should can't login if user not registered yet`, async function () {
        const userBody = {
            email: internet.email(),
            password: internet.password(12),
        }

        const response = await request(this.app)
            .post('/login')
            .send(userBody)
        expect(response.status).is.equal(404)

    })

    it(`should can't login if user set wrong password`, async function () {
        const userBody = {
            fullName: name.findName(),
            email: internet.email(),
            password: internet.password(12),
        }

        await this.db.users.create(userBody)
        userBody.password = internet.password(12)

        const response = await request(this.app)
            .post('/login')
            .send(userBody)

        expect(response.status).is.equal(400)

    })

})
