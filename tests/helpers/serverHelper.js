require('dotenv').config()
const { random } = require("faker");
const getPort = require('get-port');
const execute = require('./executeUtil');
const readdirp = require('readdirp')
const path = require('path')

class ServerHelper {
    constructor(routePath) {
        this.routePath = routePath
        this.dbName = random.alphaNumeric(10)
    }
    async start() {
        process.env.DB_DATABASE = this.dbName
        this.db = require("../../models");
        await execute('sequelize db:create')
        await execute('sequelize db:migrate')

        const express = require('express')
        const app = express()
        app.use(express.json())

        //** Route loader */
        readdirp('.', { fileFilter: 'rootRoute.js' })
            .on('data', (route) => {
                app.use(require(`${route.fullPath}`))
                console.log(`Route loader:\t${path.basename(route.path)}`);
            })
        readdirp('.', { fileFilter: ['*Route.js', '!rootRoute.js'], directoryFilter: '!private' })
            .on('data', (route) => {
                app.use(require(`${route.fullPath}`))
                console.log(`Route loader:\t${path.basename(route.path)}`);
            })
        readdirp('.', { fileFilter: ['*Route.js', '!rootRoute.js'], directoryFilter: '!public' })
            .on('data', (route) => {
                app.use(require(`${route.fullPath}`))
                console.log(`Route loader:\t${path.basename(route.path)}`);
            })

        this.port = await getPort()
        this.app = app.listen(this.port)
    }
    async drop() {
        process.env.DB_DATABASE = this.dbName
        await execute('sequelize db:drop')
        this.app.close()
    }
}

module.exports = ServerHelper
