const { encryptPass, decryptPass } = require('../helpers/cryptHelper');
const CustomError = require('../helpers/customErrorHelper');
const db = require('../models');
const jwt = require('jsonwebtoken');

module.exports = {
    register: async function (req) {
        //** Check data from request body */
        if (!req.body) {
            throw new CustomError(421, "REQUEST_BODY_NEEDED", "Please use request body")
        };

        //** validate data from request body */
        let { fullName, email, password, photoUrl } = req.body;
        photoUrl = ((photoUrl === undefined) || (photoUrl === '')) ? null : photoUrl;

        //** encrypt user password */
        password = await encryptPass(password);

        //** trying create user to db */
        let returnedData;
        await db.users.create({ fullName, email, password, photoUrl })
            .then(result => {
                returnedData = result.get({ plain: true })
            })
            .catch(() => {
                throw new CustomError(409, "USER_REGISTRATION_FAILED", "User detail have been registered before")
            });

        //** serve return data */
        delete returnedData.password;

        //** serving token to returnedData */
        returnedData.token = jwt.sign(returnedData, process.env.SIMPLE_APP_SCRET_TOKEN, {
            expiresIn: '6h'
        });

        return returnedData
    },
    login: async function (req) {
        //** Check data from request body */
        if (!req.body) {
            throw new CustomError(421, "REQUEST_BODY_NEEDED", "Please use request body")
        };

        //** validate data from request body */
        let { email, password } = req.body;

        //** find user data */
        const user = await db.users.findOne({
            raw: true,
            where: { email }
        });
        if (!user) {
            throw new CustomError(404, "USER_NOT_FOUND", "User not registered yet")
        };

        //** decrypting user password and check password */
        const isPassMatch = await decryptPass(password, user.password)
        if (!isPassMatch) {
            throw new CustomError(400, "PASS_NOT_MATCH", "Wrong user Password")
        };

        //** serving returned data */
        delete user.password

        //** serving token to user */
        user.token = jwt.sign(user, process.env.SIMPLE_APP_SCRET_TOKEN, {
            expiresIn: '6h'
        });

        return user
    }
}