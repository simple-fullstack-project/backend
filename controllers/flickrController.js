const axios = require('axios');
const CustomError = require('../helpers/customErrorHelper');

module.exports = {
    getFlickrFeed: async function () {
        //** try to catch feed from flickr */
        const flickrFeedApiUrl = 'https://api.flickr.com/services/feeds/photos_public.gne'
        const result = await axios.get(`${flickrFeedApiUrl}?format=json&nojsoncallback=1`)

        //** collecting media from flickr */
        const media = []
        result.data.items.forEach(item => {
            media.push(item.media.m)
        });

        return { result: media };
    },
    getFlickrTag: async function (req) {
        //** validate params */
        const { tag } = req.params
        if (!tag) {
            throw new CustomError(405, "PARAMS_IN_PATH_NEEDED", "Please insert tag in path parameters")
        };
        //** try to catch image using tag from flickr */
        const flickrFeedApiUrl = 'https://api.flickr.com/services/feeds/photos_public.gne'
        const result = await axios.get(`${flickrFeedApiUrl}?tags=${tag}&format=json&nojsoncallback=1`)

        //** collecting media from flickr */
        const media = []
        result.data.items.forEach(item => {
            media.push(item.media.m)
        });

        return { result: media };
    }
}