module.exports = function (err, req, res, next) {
    if (!['TokenExpiredError', 'JsonWebTokenError'].includes(err.name)) {
        if (!err.code)
            return res.status(500).json({
                status: 500,
                code: "ERR_SERVER",
                message: err.message,
                stack: err.stack
            })
        if (err.code != 500)
            return res.status(err.code).json(err);
        else
            return res.status(500).json({
                status: 500,
                code: "ERR_SERVER",
                message: err.message,
                stack: err.stack
            })
    }
}