const jwt = require('jsonwebtoken')
const passport = require('passport')
const { Strategy } = require('passport-http-bearer')

const secret = process.env.SIMPLE_APP_SCRET_TOKEN

passport.use(new Strategy(
    (token, done) => {
        jwt.verify(token, secret, (err, decoded) => {
            if (err) {
                done(err)
            }
            done(null, decoded)
        })
    }))



module.exports = passport