//** Load configuration value from environment */
require('dotenv').config();

let { SIMPLE_APP_DB, DB_USERNAME, DB_PASSWORD, DB_HOST } = process.env
SIMPLE_APP_DB = (SIMPLE_APP_DB === undefined) ? "Simple App Database" : SIMPLE_APP_DB;
DB_USERNAME = (DB_USERNAME === undefined) ? "root" : DB_USERNAME;
DB_PASSWORD = (DB_PASSWORD === undefined) ? "" : DB_PASSWORD;
DB_HOST = (DB_HOST === undefined) ? "127.0.0.1" : DB_HOST;

module.exports = {
  "development": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": SIMPLE_APP_DB,
    "host": DB_HOST,
    "dialect": "mysql",
    "logging": false,
    "connectTimeout": 15000
  },
  "production": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": SIMPLE_APP_DB,
    "host": DB_HOST,
    "dialect": "mysql",
    "logging": false
  }
}
