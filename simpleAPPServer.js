require('dotenv').config()
const express = require('express')
const passport = require('passport')
const readdirp = require('readdirp')
const path = require('path')
const cors = require('cors')

const app = express()

//** Express application/json parser */
app.use(express.json())

app.use(passport.initialize());

//** CORS OPTION */
const corsOptionsDelegate = function (req, callback) {
    callback(null, { origin: true })
}
app.use(cors(corsOptionsDelegate))

//** Route loader */
readdirp('.', { fileFilter: 'rootRoute.js' })
    .on('data', (route) => {
        app.use(require(`./${route.path}`))
        console.log(`Route loader:\t${path.basename(route.path)}`);
    })
readdirp('.', { fileFilter: ['*Route.js', '!rootRoute.js'], directoryFilter: '!private' })
    .on('data', (route) => {
        app.use(require(`./${route.path}`))
        console.log(`Route loader:\t${path.basename(route.path)}`);
    })
readdirp('.', { fileFilter: ['*Route.js', '!rootRoute.js'], directoryFilter: '!public' })
    .on('data', (route) => {
        app.use(require(`./${route.path}`))
        console.log(`Route loader:\t${path.basename(route.path)}`);
    })

//** Starting express listener */
const PORT = (process.env.SIMPLE_APP_PORT === undefined) ? 3000 : process.env.SIMPLE_APP_PORT;
const HOSTNAME = (process.env.SIMPLE_APP_HOSTNAME === undefined) ? `http://localhost:${PORT}` : process.env.SIMPLE_APP_HOSTNAME;
app.listen(PORT, () => {
    console.log(`The App start and listening in ${HOSTNAME}`);
})