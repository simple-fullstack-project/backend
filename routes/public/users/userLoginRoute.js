const express = require('express')
const userController = require('../../../controllers/userController')
const errorMiddleware = require('../../../middlewares/errorMiddleware')

const app = express.Router()

app.post('/login', async (req, res, next) => {
    try {
        const result = await userController.login(req)
        res.status(200).json(result)
    } catch (error) {
        next(error)
    }
})

app.use(errorMiddleware)

module.exports = app
