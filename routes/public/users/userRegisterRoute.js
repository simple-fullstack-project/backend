const express = require('express')
const userController = require('../../../controllers/userController')
const errorMiddleware = require('../../../middlewares/errorMiddleware')

const app = express.Router()

app.post('/register', async (req, res, next) => {
    try {
        const result = await userController.register(req)
        res.status(200).json(result)
    } catch (error) {
        next(error)
    }
})

app.use(errorMiddleware)

module.exports = app
