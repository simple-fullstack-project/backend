const express = require('express')

const app = express.Router()

app.get('/', (_, res) => {
    res.status(200).send('HELLO WORLD! MY SIMPLE APP IS HERE')
})

module.exports = app
