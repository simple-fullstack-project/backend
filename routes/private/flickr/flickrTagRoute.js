const express = require('express')
const flickrController = require('../../../controllers/flickrController')
const errorMiddleware = require('../../../middlewares/errorMiddleware')
const passport = require('../../../middlewares/authMiddleware')

const app = express.Router()

app.get('/flickr-search/:tag', passport.authenticate('bearer', { session: false }), async (req, res, next) => {
    try {
        const result = await flickrController.getFlickrTag(req)
        res.status(200).json(result)
    } catch (error) {
        next(error)
    }
})

app.use(errorMiddleware)

module.exports = app
